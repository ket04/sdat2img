#!/usr/bin/env python
# -*- coding: utf-8 -*-
#====================================================
#          DOSYA: sdat2img.py
#       GELİŞTİRİCİLER: xpirt - luxi78 - howellzhu
#          TARİH: 2017-01-04 2:01:45 CEST
#          ÇEVİRİ: TheDoop
#====================================================

import sys, os, errno

__version__ = '1.0'

if sys.hexversion < 0x02070000:
    print >> sys.stderr, "Python 2.7 or newer is required."
    try:
       input = raw_input
    except NameError: pass
    input('Press ENTER to exit...')
    sys.exit(1)
else:
    print('sdat2img binary - version: %s\n' % __version__)

try:
    TRANSFER_LIST_FILE = str(sys.argv[1])
    NEW_DATA_FILE = str(sys.argv[2])
except IndexError:
    print('\nUsage: sdat2img.py <transfer_list> <system_new_file> [system_img]\n')
    print('    <transfer_list>: transfer list file')
    print('    <system_new_file>: system new dat file')
    print('    [system_img]: output system image\n\n')
    print('Visit xda thread for more information.\n')
    try:
       input = raw_input
    except NameError: pass
    input('Press ENTER to exit...')
    sys.exit()

try:
    OUTPUT_IMAGE_FILE = str(sys.argv[3])
except IndexError:
    OUTPUT_IMAGE_FILE = 'system.img'

BLOCK_SIZE = 4096

def rangeset(src):
    src_set = src.split(',')
    num_set =  [int(item) for item in src_set]
    if len(num_set) != num_set[0]+1:
        print('Error on parsing following data to rangeset:\n%s' % src)
        sys.exit(1)

    return tuple ([ (num_set[i], num_set[i+1]) for i in range(1, len(num_set), 2) ])

def parse_transfer_list_file(path):
    trans_list = open(TRANSFER_LIST_FILE, 'r')

    # Transfer listesindeki ilk satır, sürüm numarasıdır
    version = int(trans_list.readline())

    # Transfer listesindeki ikinci satır, yazmayı beklediğimiz blokların toplam sayısıdır
    new_blocks = int(trans_list.readline())

    if version >= 2:
        # Third line is how many stash entries are needed simultaneously
        trans_list.readline()
        # Dördüncü satır, eşzamanlı olarak saklanacak maksimum blok sayısıdır
        trans_list.readline()

    #Sonraki satırlar, tüm bireysel aktarma komutlarıdır
    commands = []
    for line in trans_list:
        line = line.split(' ')
        cmd = line[0]
        if cmd in ['erase', 'new', 'zero']:
            commands.append([cmd, rangeset(line[1])])
        else:
            #Sayılarla başlayan satırları atlayın,komut değildirler.
            if not cmd[0].isdigit():
                print('Command "%s" is not valid.' % cmd)
                trans_list.close()
                sys.exit(1)

    trans_list.close()
    return version, new_blocks, commands

def main(argv):
    version, new_blocks, commands = parse_transfer_list_file(TRANSFER_LIST_FILE)

    if version == 1:
        print('Android Lollipop 5.0 detected!\n')
    elif version == 2:
        print('Android Lollipop 5.1 detected!\n')
    elif version == 3:
        print('Android Marshmallow 6.x detected!\n')
    elif version == 4:
        print('Android Nougat 7.x / Oreo 8.x detected!\n')
    else:
        print('Unknown Android version!\n')

    #Kazayla oluşan veri kaybını önlemek için var olan dosyaları gizlemeyin
    try:
        output_img = open(OUTPUT_IMAGE_FILE, 'wb')
    except IOError as e:
        if e.errno == errno.EEXIST:
            print('Error: the output file "{}" already exists'.format(e.filename))
            print('Remove it, rename it, or choose a different file name.')
            sys.exit(e.errno)
        else:
            raise

    new_data_file = open(NEW_DATA_FILE, 'rb')
    all_block_sets = [i for command in commands for i in command[1]]
    max_file_size = max(pair[1] for pair in all_block_sets)*BLOCK_SIZE

    for command in commands:
        if command[0] == 'new':
            for block in command[1]:
                begin = block[0]
                end = block[1]
                block_count = end - begin
                print('Copying {} blocks into position {}...'.format(block_count, begin))

                # Çıktı dosyası Konumu
                output_img.seek(begin*BLOCK_SIZE)
                
                # Bir kerede bir blok kopyalar
                while(block_count > 0):
                    output_img.write(new_data_file.read(BLOCK_SIZE))
                    block_count -= 1
        else:
            print('Skipping command %s...' % command[0])

    # Gerekirse dosyayı daha büyük yapın
    if(output_img.tell() < max_file_size):
        output_img.truncate(max_file_size)

    output_img.close()
    new_data_file.close()
    print('Done! Output image: %s' % os.path.realpath(output_img.name))

if __name__ == '__main__':
    main(sys.argv)
