# sdat2img
Android data imajı'nı (.dat) Ext4 Dosya sistemi imajı'na (.img) Dönüştürür



## Gereksinimler
Python 2.7 veya daha yeni bir sürümü

## Desteklenen işletyim sistemleri
Windows
Linux
MacOS
ARM


## Kullanımı
```
sdat2img.py <transfer_list> <system_new_file> [system.img]
```
- `<transfer_list>` = Zip içerisindeki system.transfer.list dosyasını ekleyin
- `<system_new_file>` = Zip içerisindeki system.new.dat Dosyasını ekleyin
- `[system_img]` = Çıkış dosyayı (isteğe bağlı)



## Örnek
Bu bir Linux sistemi üzerinde basit bir örnek:
```
~$ ./sdat2img.py system.transfer.list system.new.dat system.img
```
